package com.szatmari.dev.datanotification;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private static final int READ_PHONE_STATE_REQUEST = 37;
    private static final int PACKAGE_USAGE_STATS_REQUEST = 38;

    NotificationCompat.Builder mBuilder;
    NotificationManagerCompat notificationManager;
    String DeviceSum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestPhoneStateStats();
        DeviceSum = "hello";
        getMidnight();
        //DeviceSum = getMidnight().toString();

        getDataUsage(getMidnight());
        createNotificationChannel();
        createNotification();
        notificationManager = NotificationManagerCompat.from(this);

    }

    private String getSubscriberId(Context context, int networkType) {
        if (ConnectivityManager.TYPE_MOBILE == networkType) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return "";
            }
            return tm.getSubscriberId();
        }
        return "";
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(getString(R.string.ch_id), name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void createNotification() {
        mBuilder = new NotificationCompat.Builder(this, getString(R.string.ch_id))
                .setSmallIcon(R.drawable.ic_stat_network_cell)
                .setContentTitle(DeviceSum)
                .setShowWhen(false)
                .setOngoing(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
    }

    public  void updateNotification(View view){
        if( ((CheckBox)view).isChecked() == true){
            // notificationId is a unique int for each notification that you must define
            notificationManager.notify(R.integer.Notification_ID, mBuilder.build());
        } else {
            notificationManager.cancel(R.integer.Notification_ID);
        }
        Log.d("INFO","checkbox state changed");
    }

    private void requestPhoneStateStats() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE_REQUEST);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.PACKAGE_USAGE_STATS}, PACKAGE_USAGE_STATS_REQUEST);
    }

    private String getDataUsage(long startTime) {
        NetworkStatsManager networkStatsManager = (NetworkStatsManager) getApplicationContext().getSystemService(Context.NETWORK_STATS_SERVICE);
        try {
            Long Rx = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(this, ConnectivityManager.TYPE_MOBILE),
                    startTime,
                    System.currentTimeMillis()).getRxBytes();
            Long Tx = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(this, ConnectivityManager.TYPE_MOBILE),
                    startTime,
                    System.currentTimeMillis()).getTxBytes();
            Double Sum = (Tx + Rx)/(1024.0*1024.0);
            DeviceSum = Sum.toString() + " MB";
        } catch (Exception e) {
            e.printStackTrace();
            DeviceSum = "null";
        }
        return DeviceSum;
    }

    private Long getMidnight(){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTimeInMillis();
    }
}
